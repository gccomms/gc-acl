# GC Access Control

GC Access Control is a [WordPress](https://wordpress.org) plugin that enables a more granular control over roles and page permissions. Used in conjunction with the [Members](https://wordpress.org/plugins/members/) plugin, it turns your WordPress environment into an enterprise content management system. GC Access Control is proudly maintained by the Office of Communications and Marketing at [The Graduate Center, CUNY](https://www.gc.cuny.edu/) and is released under the [MIT License](LICENSE).

## Objectives

* Allow administrators to assign specific **pages** to users (no other post types are supported at this time)
* Create permission groups to streamline access management tasks.
* Seamless WordPress admin integration.
* Simplify the admin interface for regular editors by removing unnecessary modules and sections.

## Software Requirements
* [Advanced Custom Fields](https://www.advancedcustomfields.com/)
* Optional: [Members](https://wordpress.org/plugins/members/)

## Getting Started

You can install this plugin either by [downloading the zip file](https://gitlab.com/gccomms/gc-acl/-/archive/master/gc-acl-master.zip) or via command line by cloning the repository under **wp-content/plugins**:

`git clone -b master https://gitlab.com/gccomms/gc-acl.git /var/www/html/wp-content/plugins/gc-acl/`

After you activate the plugin, you will see a new section in the Edit User screen, with three selection boxes:

* `Groups` can be used to assign a given user to a group. Any permissions associated to that group will be inherited by all the users who belong to it.
* `Assigned pages` will let you determine what pages this user should have access to. By default, if this field is empty, users will have access to all the pages on your website. By leveraging the functionality provided by the [Members](https://wordpress.org/plugins/members/) plugin, you can then customize what capabilities this user will be granted on those pages. This gives administrators ultimate control over their users.
* `Excluded pages` does what the label implies: it creates an exclusion list to prevent users from accessing certain pages.

You can create new Groups under `Users > Groups`. The plugin determines the list of pages a given user has access to by following these steps:

1. Are there any pages assigned to this user?
	1. Yes: use this set as the starting point.
	1. No: use 'all pages' as the starting point.
1. If this user belongs to any groups
	1. Add the 'Assigned Pages' to the set
	1. Remove the 'Excluded Pages' from the set
1. If this user's Excluded Pages list is not empty
	1. Remove those pages from the set

## Recommended Tools

In order to enhance your WordPress environment and enable features that will turn it into an enterprise content management system, we suggest installing the following software:

* [Graduate Vagrant](https://gitlab.com/gccomms/vagrant): a virtual development environment based on modern best practices.
* [WIBE](https://gitlab.com/gccomms/wordpress): a WordPress Boilerplate Environment that separates your code from all the necessary dependencies.
* [Nested Pages](https://gitlab.com/gccomms/wp-nested-pages): our fork of the popular Nested Pages plugin, which allows users to better navigate your site tree.