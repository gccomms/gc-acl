<?php
/**
 * Plugin Name: GC Access Control
 * Description: Enables features to restrict user access to given pages, features and components. It requires ACF and Members.
 * Version: 1.3
 * Author: The Graduate Center, CUNY
 * Author URL: https://www.gc.cuny.edu
 * License: GPL2
 */


class gc_acl{
	public static $user_pages = array();

  	public static function init() {
		// This plugin needs ACF to be enabled
		if ( !function_exists( 'get_field' ) ) {
			return true;
		}

		$blog_id = get_current_blog_id();
		if ( empty( $blog_id ) ) {
			$blog_id = 1;
		}

		add_filter( 'user_has_cap', array( __CLASS__, 'user_caps' ), 10, 3 );

		// Add ACF Field to the User Profile page
		add_action( 'admin_init', array( __CLASS__, 'add_acf_fields' ) );
		add_action( 'init', array( __CLASS__, 'init_user_pages' ), 100 );

		// Show the Page ID on the relationship field
		add_filter( 'acf/fields/relationship/result/name=access_control_settings_pages_' . $blog_id, array( __CLASS__, 'add_page_id_to_field' ), 10, 2 );
		add_filter( 'acf/fields/relationship/query/name=access_control_settings_pages_' . $blog_id, array( __CLASS__, 'modify_acf_relationship_search_query' ), 10, 3 );
		add_filter( 'acf/fields/relationship/result/name=access_control_excluded_pages_' . $blog_id, array( __CLASS__, 'add_page_id_to_field' ), 10, 2 );
		add_filter( 'acf/fields/relationship/query/name=access_control_excluded_pages_' . $blog_id, array( __CLASS__, 'modify_acf_relationship_search_query' ), 10, 3 );
		
		// Show User Roles and Capabilities
		add_filter( 'acf/load_field/name=access_control_settings_role', array( __CLASS__, 'populate_user_roles' ) );
		add_filter( 'acf/load_field/name=user_access_ownership_role', array( __CLASS__, 'populate_user_roles' ) );
		add_filter( 'acf/load_field/name=user_access_capabilities', array( __CLASS__, 'populate_user_capabilities' ) );

		// Redirect all users to the Pages screen
		add_filter( 'login_redirect', array( __CLASS__, 'redirect_to_pages_view' ) );

		// Remove unnecessary admin pages for non-admins
		add_action( 'admin_menu', array( __CLASS__, 'restrict_non_admins' ), 100 );

		// Enqueue a stylesheet to hide certain features, if needed
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_styles' ) );

		// Add the user's role as a BODY class in the admin
		add_filter( 'admin_body_class', array( __CLASS__, 'admin_body_class' ), 100 );

		// Remove Revolution Slider and other metaboxes/features from the Edit Page/Post screen
		add_action( 'do_meta_boxes', array( __CLASS__, 'remove_meta_boxes' ) );
		add_filter( 'theme_page_templates', array( __CLASS__, 'remove_page_templates' ), 20, 3 );

		// // Define the Groups CPT and add it to the Users menu
		add_action( 'init', array( __CLASS__, 'register_groups_post_type' ) );
		add_action( 'add_meta_boxes', array( __CLASS__, 'add_user_list_metabox_to_groups' ) );
		add_filter( 'manage_users_columns', array( __CLASS__, 'add_group_to_user_table' ) );
		add_filter( 'manage_users_custom_column', array( __CLASS__, 'add_group_to_user_table_row' ), 10, 3 );
		add_filter( 'post_updated_messages', array( __CLASS__, 'groups_updated_messages' ) );

		// Notify an administrator when a page is submitted for approval
	}

	public static function init_user_pages() {
		// Init arrays used to remove duplicates
		$included_pages = array();
		$excluded_pages = array();

		// ACF Field Keys are associated to a specific blog in a multisite environment
		$blog_id = get_current_blog_id();
		if ( empty( $blog_id ) ) {
			$blog_id = 1;
		}

		$user_id = get_current_user_id();

		// Administrators have access to the entire site tree, no matter what
		if ( !current_user_can( 'manage_options' ) || current_user_can( 'edit_all_pages' ) ) {
			// If this user belongs to one or more groups, add all the group pages
			$user_groups = get_field( 'user_access_groups_' . $blog_id, 'user_' . $user_id );
			if ( is_array( $user_groups ) ) {
				foreach ( $user_groups as $a_group_id ) {
					$included_group_pages = get_field( 'access_control_settings_pages_' . $blog_id, $a_group_id );
					if ( is_array( $included_group_pages ) && !empty( $included_group_pages ) ) {
						$included_pages = array_merge( $included_pages, $included_group_pages );
					}

					$excluded_group_pages = get_field( 'access_control_excluded_pages_' . $blog_id, $a_group_id );
					if ( !empty( $excluded_group_pages ) && is_array( $excluded_group_pages ) ) {
						$excluded_pages = array_merge( $excluded_pages, $excluded_group_pages );
					}
				}
			}

			// Users can also access pages assigned specifically to them
			$included_user_pages = get_field( 'access_control_settings_pages_' . $blog_id, 'user_' . $user_id );
			if ( !empty( $included_user_pages ) && is_array( $included_user_pages ) ) {
				$included_pages = array_merge( $included_pages, $included_user_pages );
			}

			// Users can also access pages assigned specifically to them
			$excluded_user_pages = get_field( 'access_control_excluded_pages_' . $blog_id, 'user_' . $user_id );
			if ( !empty( $excluded_user_pages ) && is_array( $excluded_user_pages ) ) {
				$excluded_pages = array_merge( $excluded_pages, $excluded_user_pages );
			}

			// Permissions are inherited by all the child pages
			foreach ( $included_pages as $a_page_id ) {
				$child_pages = get_pages( // get_pages cannot return just post IDs, it can only return arrays of objects
					array(
						'child_of' => $a_page_id,
						'post_status' => 'publish,private,draft',
					)
				);

				if ( is_array( $child_pages ) ) {
					foreach ( $child_pages as $a_child_page ) {
						$included_pages[] = $a_child_page->ID;
					}
				}
			}

			foreach ( $excluded_pages as $a_page_id ) {
				$child_pages = get_pages( // get_pages cannot return just post IDs, it can only return arrays of objects
					array(
						'child_of' => $a_page_id,
						'post_status' => 'publish,private,draft',
					)
				);

				if ( is_array( $child_pages ) ) {
					foreach ( $child_pages as $a_child_page ) {
						$excluded_pages[] = $a_child_page->ID;
					}
				}
			}
		}

		if ( empty( $included_pages ) ) {
			self::$user_pages = get_posts( // get_posts can return just post IDs, instead of entire objects
				array(
					'post_status' => 'publish,private,draft',
					'fields' => 'ids',
					'posts_per_page'  => -1,
					'post_type' => 'page'
				)
			);
		}
		else {
			self::$user_pages = $included_pages;
		}

		// Remove excluded pages
		self::$user_pages = array_diff( self::$user_pages, $excluded_pages );

		// Remove duplicates
		self::$user_pages = array_unique( self::$user_pages );
	}

	public static function user_caps( $user_caps, $required_cap, $args ) {

		// // Administrators are not limited by per-page permissions
		if ( ( isset( $user_caps[ 'manage_options' ] ) && $user_caps[ 'manage_options' ] === true ) || empty( $args[ 1 ] ) ) {
			return $user_caps;
		}

		// // Regular users cannot upload media through the Media Library, they can only attach them to pages
		if ( strpos( $_SERVER[ 'REQUEST_URI' ], 'media-new.php' ) !== false ) {
			$user_caps;
			return $user_caps;
		}
		// // ... and this is the exception that allows them to attach them to a page (edit_post is needed)
		else if ( strpos( $_SERVER[ 'REQUEST_URI' ], 'async-upload.php' ) !== false && !empty( $args[ 2 ] ) ) {
			$user_caps[ 'edit_post' ] = true;
			return $user_caps;
		}

		// $args[ 2 ], if defined, contains the post_id of the page we want to check
		if ( !empty( $args[ 2 ] ) && !empty( self::$user_pages ) && !in_array( $args[ 2 ], self::$user_pages ) ) {
			$user_caps[ 'edit_pages' ] = false;
			$user_caps[ 'edit_others_pages' ] = false;
			$user_caps[ 'edit_private_pages' ] = false;
			$user_caps[ 'edit_published_pages' ] = false;
		}

		return $user_caps;
	}

	// Add ID info to ACF field
	public static function add_page_id_to_field( $html, $post ) {
		$parent_title = !empty( $post->post_parent ) ? ' - Parent: ' . get_the_title( $post->post_parent ) . ', ID: ' . $post->post_parent : '';
	  	return $html . " (ID: {$post->ID}{$parent_title})";
	}

	// Tweak ACF search to look only at page titles and IDs
	public static function search_custom_meta_acf_alter_search( $search, $query ) {
		if ( empty( $_POST[ 's' ] ) ) {
			return '';
		}

		$my_search = array();
		foreach ( explode( ' ', $_POST[ 's' ] ) as $term ) {
			$term = esc_sql( like_escape( $term ) );
			if ( is_numeric( $term ) ) {
				$my_search[] = $GLOBALS[ 'wpdb' ]->posts . ".ID = $term";
			}
			else {
				$my_search[] = $GLOBALS[ 'wpdb' ]->posts . ".post_title LIKE '%$term%'";
			}
		}

		$search = implode( ' AND ', $my_search );

		return ' AND ' . $search;
	}

	// Hook function to customize the ACF field search results
	public static function modify_acf_relationship_search_query( $args, $field, $post ) {
		add_filter( 'posts_search', array( __CLASS__, 'search_custom_meta_acf_alter_search'), 10, 2 );
		return $args;
	}

	public static function add_group_to_user_table( $columns ) {
		$columns = self::_array_insert_after( $columns, 'role', 'gc_user_groups', 'Groups' );
		return $columns;
	}

	public static function add_group_to_user_table_row( $val, $column_name, $user_id ) {
		// We need to save multiple copies of each field, one for each site in a multisite setup
		$blog_id = get_current_blog_id();
		if ( empty( $blog_id ) ) {
			$blog_id = 1;
		}

		$user_groups = get_field( 'user_access_groups_' . $blog_id, 'user_' . $user_id );

		if ( $column_name == 'gc_user_groups' ) {
			if ( empty( $user_groups ) || !is_array( $user_groups ) ) {
				return '&mdash;';
			}

			$group_names = array();
			foreach ( $user_groups as $a_group_id ) {
				$a_group_obj = get_post( $a_group_id );
				if ( is_object( $a_group_obj ) ) {
					$group_names[] = '<a href="' . get_edit_post_link($a_group_id) . '">' . $a_group_obj->post_title . '</a>';
				}
			}
			return implode( ', ', $group_names );
		}

		return $val;
	}

	public static function enqueue_styles() {
		wp_register_style( 'gc-acl', plugins_url( 'style.css', __FILE__ ) );
		wp_enqueue_style( 'gc-acl' );
	}

	public static function admin_body_class( $_class = '' ) {
		if ( !empty( $GLOBALS[ 'current_user']->roles[ 0 ] ) ) {
			$clean_user_role = 'user-role-' . str_replace( '_', '-', $GLOBALS[ 'current_user']->roles[ 0 ] );
			return "$_class $clean_user_role";
		}

		return $_class;
	}

	public static function redirect_to_pages_view( $url = '' ) {
		return is_plugin_active( 'wp-nested-pages/nestedpages.php' ) ? admin_url( 'admin.php?page=nestedpages' ) : admin_url( 'edit.php?post_type=page' );
	}

	public static function restrict_non_admins() {
		// Hide Comments menu if they're disabled in the settings
		if ( get_option( 'default_comment_status' ) !== 'open' ) {
			remove_menu_page( 'edit-comments.php' );
		}
		
		if ( current_user_can( 'manage_options' ) ) {
			return 0;
		}

		// Tools
		remove_menu_page( 'tools.php' );

		// WPBakery Welcome Page
		remove_menu_page( 'vc-welcome' );
	}

	public static function add_acf_fields() {
		// We need to save multiple copies of each field, one for each site in a multisite setup
		$blog_id = get_current_blog_id();
		if ( empty( $blog_id ) ) {
			$blog_id = 1;
		}

		acf_add_local_field_group( array(
			'key' => 'group_5592e6aec320e',
			'title' => 'User Permissions',
			'fields' => array(
				array(
					'key' => 'field_55953f359c02b' . $blog_id,
					'label' => 'Groups',
					'name' => 'user_access_groups_' . $blog_id,
					'type' => 'relationship',
					'instructions' => 'Specify if this user belongs to any groups. If no groups are selected, and no pages are assigned to this user using the field below, this user will have access to ALL pages.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => ''
					),
					'post_type' => array(
						0 => 'gc_user_groups'
					),
					'taxonomy' => array(
					),
					'filters' => array(
						0 => 'search'
					),
					'elements' => '',
					'min' => 0,
					'max' => '',
					'return_format' => 'id'
				)
			),
			'location' => array(
				array(
					array(
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator'
					),
					array(
						'param' => 'user_form',
						'operator' => '==',
						'value' => 'edit'
					)
				)
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => ''
		));

		acf_add_local_field_group( array(
			'key' => 'group_558441c8ab846',
			'title' => 'Pages',
			'fields' => array(
				array(
					'key' => 'field_5592f8883ef68' . $blog_id,
					'label' => 'Assigned pages',
					'name' => 'access_control_settings_pages_' . $blog_id,
					'type' => 'relationship',
					'instructions' => 'Define the list of pages associated to this user/group. All child pages will be inherited as well. Users have access to the entire site tree by default.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array(
						0 => 'page',
					),
					'taxonomy' => array(
					),
					'filters' => array(
						0 => 'search',
					),
					'elements' => '',
					'min' => '',
					'max' => '',
					'return_format' => 'id'
				),
				array(
					'key' => 'field_5bcf608fd1c98',
					'label' => 'Excluded pages',
					'name' => 'access_control_excluded_pages_' . $blog_id,
					'type' => 'relationship',
					'instructions' => 'You can override the inheritance of capabilities defined by the previous field by creating an exclusion list here below.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array(
						0 => 'page',
					),
					'taxonomy' => '',
					'filters' => array(
						0 => 'search',
					),
					'elements' => '',
					'min' => '',
					'max' => '',
					'return_format' => 'id',
				)
			),
			'location' => array(
				array(
					array(
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator'
					),
					array(
						'param' => 'user_form',
						'operator' => '==',
						'value' => 'edit'
					)
				),
				array(
					array(
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator'
					),
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'gc_user_groups'
					)
				)
			),
			'menu_order' => 20,
			'position' => 'normal',
			'style' => 'seamless',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => ''
		));

		// acf_add_local_field_group(array(
		// 	'key' => 'group_5a625648623ba',
		// 	'title' => 'Taxonomies',
		// 	'fields' => array(
		// 		array(
		// 			'key' => 'field_5a6256ba32d40',
		// 			'label' => 'Taxonomies',
		// 			'name' => 'csl_grant_access',
		// 			'type' => 'advanced_taxonomy_selector',
		// 			'instructions' => 'Select one or more terms to restrict this user to only access posts associated to those terms. Capabilities are defined by the role assigned to this user.',
		// 			'required' => 0,
		// 			'conditional_logic' => 0,
		// 			'wrapper' => array(
		// 				'width' => '',
		// 				'class' => '',
		// 				'id' => '',
		// 			),
		// 			'data_type' => 'terms',
		// 			'taxonomies' => array(
		// 				0 => 'all',
		// 			),
		// 			'post_type' => '',
		// 			'field_type' => 'multiselect',
		// 			'allow_null' => 1,
		// 			'return_value' => 'term_id',
		// 		),
		// 	),
		// 	'location' => array(
		// 		array(
		// 			array(
		// 				'param' => 'current_user_role',
		// 				'operator' => '==',
		// 				'value' => 'administrator',
		// 			),
		// 			array(
		// 				'param' => 'user_form',
		// 				'operator' => '==',
		// 				'value' => 'edit',
		// 			),
		// 		),
		// 	),
		// 	'menu_order' => 30,
		// 	'position' => 'normal',
		// 	'style' => 'seamless',
		// 	'label_placement' => 'top',
		// 	'instruction_placement' => 'label',
		// 	'hide_on_screen' => '',
		// 	'active' => 1,
		// 	'description' => '',
		// ));
	}

	public static function populate_user_roles( $field ) {	
		// reset choices
		$field[ 'choices' ] = array( 'default_for_user' => 'Default Role' );
		
		foreach ( $GLOBALS[ 'wp_roles' ]->roles as $a_role_slug => $a_role ) {
			// List all non-admin roles
			if ( empty( $a_role[ 'capabilities' ][ 'manage_options' ] ) ) {
				$field['choices'][ $a_role_slug ] = $a_role[ 'name' ];
			}
		}

		return $field;
	}

	public static function populate_user_capabilities( $field ) {	
		// reset choices
		$field[ 'choices' ] = array();

		foreach ( $GLOBALS[ 'wp_roles' ]->roles[ 'administrator' ][ 'capabilities' ] as $a_capability_slug => $is_capability_enabled ) {
			$field['choices'][ $a_capability_slug ] = $a_capability_slug;
		}

		return $field;
	}

	public static function register_groups_post_type() {
		register_post_type( 'gc_user_groups',
		    array(
	            'labels' => array(
					'name' => 'Groups',
					'singular_name' => 'Group',
					'add_new_item' => 'Add New Group',
					'new_item' => 'New Group',
					'edit_item' => 'Edit Group',
					'view_item' => 'View Group',
					'not_found' => 'No groups found',
					'not_found_in_trash' => 'No groups found in Trash'
	            ),
			    'has_archive' => false,
			    'public' => false,
			    'show_ui' => true,
			    'show_in_menu' => 'users.php',
			    'supports' => array( 'title' ),
			    'capabilities' 	 => array(
					'publish_posts' => 'manage_options',
					'edit_posts' => 'manage_options',
					'edit_others_posts' => 'manage_options',
					'delete_posts' => 'manage_options',
					'delete_others_posts' => 'manage_options',
					'read_private_posts' => 'manage_options',
					'edit_post' => 'manage_options',
					'delete_post' => 'manage_options',
					'read_post' => 'manage_options'
				)
		    )
		);
	}

	/**
	 * Update messages.
	 *
	 * See /wp-admin/edit-form-advanced.php
	 *
	 * @param array $messages Existing post update messages.
	 *
	 * @return array Amended post update messages with new CPT update messages.
	 */
	public static function groups_updated_messages( $messages ) {
		$post = get_post();
		$post_type = get_post_type( $post );
		$post_type_object = get_post_type_object( $post_type );

		$messages[ 'gc_user_groups' ] = array(
			0  => '', // Unused. Messages start at index 1.
			1  => 'Group updated.',
			4  => 'Group updated.',
			6  => 'Group published.',
			7  => 'Group saved.',
			8  => 'Group submitted.'
		);

		if ( $post_type_object->publicly_queryable && 'gc_user_groups' === $post_type ) {
			$permalink = get_permalink( $post->ID );

			$view_link = sprintf(' <a href="%s">View Group</a>', esc_url( $permalink ) );
			$messages[ $post_type ][1] .= $view_link;
			$messages[ $post_type ][6] .= $view_link;
		}

		return $messages;
	}

	public static function add_user_list_metabox_to_groups() {
		add_meta_box( 'group_user_list', 'Users in this group', array( __CLASS__, 'list_users_metabox' ), 'gc_user_groups' );
	}

	public static function remove_meta_boxes() {
		// Revolution Slider on pages, posts, and groups
		
		remove_meta_box( 'slider_revolution_metabox', 'page', 'side' );
		remove_meta_box( 'slider_revolution_metabox', 'post', 'side' );
		remove_meta_box( 'slider_revolution_metabox', 'gc_user_groups', 'side' );

		// Page Attributes: for large sites, the Parent dropdown is not manageable
		remove_meta_box( 'pageparentdiv', 'page', 'side' );

		// Conditional metaboxes
		if ( !current_user_can( 'manage_options' ) ) {
			remove_meta_box( 'authordiv', 'page', 'side' );
			remove_meta_box( 'authordiv', 'page', 'normal' );
			remove_meta_box( 'commentsdiv', 'page', 'side' );
			remove_meta_box( 'commentsdiv', 'page', 'normal' );
			remove_meta_box( 'commentsdiv', 'post', 'side' );
			remove_meta_box( 'commentsdiv', 'post', 'normal' );
			remove_meta_box( 'commentstatusdiv', 'page', 'side' );
			remove_meta_box( 'commentstatusdiv', 'page', 'normal' );
			remove_meta_box( 'commentstatusdiv', 'post', 'side' );
			remove_meta_box( 'commentstatusdiv', 'post', 'normal' );
		}
	}

	public static function remove_page_templates( $_page_templates = array(), $_theme, $_post ) {
		if ( !is_array( $_page_templates ) ) {
			return $_page_templates;
		}

		foreach ( $_page_templates as $a_key => $a_page_template ) {
			if ( strpos( $a_page_template, 'Slider Revolution' ) !== false ) {
				unset( $_page_templates[ $a_key ] );
			}
		}

		return $_page_templates;
	}

	public static function list_users_metabox() {
		$output = array();
		$users_in_this_group = get_users( array(
			'meta_query' => array(
				array(
					'key' => 'user_access_groups', // name of custom field
					'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			),
			'orderby' => 'display_name'
		));

		foreach ( $users_in_this_group as $a_user ) {
			$output[] = '<a href="'.admin_url( 'user-edit.php?user_id=' . $a_user->ID ).'">'.$a_user->display_name.'</a> ['.$a_user->user_login.']';
		}

		if ( !empty( $output ) ) {
			echo implode( ', ', $output );
		}
		else {
			echo 'No users found.';
		}
	}

	private static function _array_insert_after( $array = array(), $key = '', $new_key = '', $new_value = '' ) {
		if ( !array_key_exists( $key, $array ) ) {
			return $array;
		}

		$new = array();
		foreach ( $array as $k => $value ) {
			$new[ $k ] = $value;
			if ( $k == $key ) {
				$new[ $new_key ] = $new_value;
			}
		}
	
		return $new;
	}
}
add_action( 'plugins_loaded', array( 'gc_acl', 'init' ), 200 );
